## Wrapsel - selenium wrapper

I think goal now is to get some sort of scrapy analog
where you have this package and you do:
```
pip install wrapsel
wrapsel init PROJECT_NAME
cd PROJECT_NAME
# write your parsers
```

In addition user will be able to write custom db modules, which is nice.
