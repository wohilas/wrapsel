from setuptools import setup

setup(name='wrapsel',
      version='0.1',
      description='selenium wrapper; for people who cannot write these selenium parsers',
      url='https://gitlab.com/wohilas/wrapsel/',
      author='Grail Finder',
      author_email='wohilas@gmail.com',
      license='BSD',
      packages=['wrapsel'],
      zip_safe=False,
      classifiers=[
          'Framework :: wrapsel',
          'Development Status :: 3 - Alpha',
          'Environment :: Console',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: BSD License',
            'Operating System :: OS Independent',
            'Programming Language :: Python',
            'Topic :: Internet :: WWW/HTTP',
            'Topic :: Software Development :: Libraries :: Application Frameworks',
            'Topic :: Software Development :: Libraries :: Python Modules',
          ],
      python_requires='>=3.6',
      install_requires=[
          'selenium>=3.1',
          ],
      include_package_data=True,
      )
