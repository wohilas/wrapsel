import os
import argparse
import pkg_resources
import wrapsel
# get all commands

# wrapsel init xbet
# xbet is arg => create dir xbet


def _init_args():
    parser = argparse.ArgumentParser(description="wrapsel arguments")
    parser.add_argument(
        "-v",
        help="print wrapsel version"
    )

    # if no arguments given - print help message
    if len(os.sys.argv)==1:
        parser.print_help(os.sys.stderr)
        os.sys.exit(1)

    args = parser.parse_args()
    return args

def _get_commands_from_entry_points(group='wrapsel.commands'):
    cmds = {}
    for entry_point in pkg_resources.iter_entry_points(group):
        obj = entry_point.load()
        if inspect.isclass(obj):
            cmds[entry_point.name] = obj()
        else:
            raise Exception("Invalid entry point %s" % entry_point.name)
    return cmds

def execute():
    cmds = _get_commands_from_entry_points()
    print(cmds)
    args = _init_args()

    # check if inside project ??
    # except of case where init project or version
    cmds = _get_commands_dict(settings, inproject)
    print(cmds)

if __name__ == "__main__":
    execute()