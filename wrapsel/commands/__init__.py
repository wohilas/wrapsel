from abc import ABC, abstractmethod

class WrapselCommand(ABC):
    pass

    @abstractmethod
    def command_syntax(self):
        """
        syntax of command call in cmd
        """
        pass

    @abstractmethod
    def parse_options(self, opts):
        """
        recognize which options is usable
        also return unrecognized ones
        (if opt is not recognized by any or used commands should be error)
        """
        pass

    @abstractmethod
    def run(self):
        """
        run command
        """
        pass
