import os
from wrapsel.commands import WrapselCommand

class Command(WrapselCommand):
    def command_syntax(self):
        return "<project_name> [project_dir]"

    def parse_options(self, opts):
        """
        get project name
        """
        pass

    def run(self):
        """
        create dir with name of project
        create setting.json inside with project settings
        parsers etc
        """
        pass
