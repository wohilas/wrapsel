from wrapsel.commands import WrapselCommand
import wrapsel

class Command(WrapselCommand):

    def command_syntax(self):
        return "[-v]"

    def parse_options(self, opts):
        """
        there should be no options for version check
        """
        assert not opts
        pass

    def run(self):
        print(wrapsel.__version__)
