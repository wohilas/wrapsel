import toml

def read_config(config_path="config.toml"):
    with open(config_path) as lc:
        config_dict = toml.load(lc)
    return config_dict

