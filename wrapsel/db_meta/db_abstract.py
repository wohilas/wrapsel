from abc import ABC, abstractmethod

class DBAbstract(ABC):

    @abstractmethod
    def write_line(self, **kwargs):
        """
        writes data line to db
        """
        pass

