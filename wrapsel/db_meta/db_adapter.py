

class DBAdapter():

    """
    Adapter class for db classes

    import and init db class
    >>> from data_keeper import DBTSV
    >>> tsv_db = DBTSV()

    then init DBAdapter and pass db instance to adapter
    with methods that you want to use
    >>> adapter = DBAdapter(tsv_db)

    now you can call its methods
    >>> adapter.write_line(key="value")
    """

    def __init__(self, obj):
        """We set the adapted methods in the object's dict"""
        self.obj = obj
        adapted_methods_2 = {m:getattr(obj, m)
                for m in obj.__dir__()
                if "__" not in m}

        self.__dict__.update(adapted_methods_2)


    def __getattr__(self, attr):
        """All non-adapted calls are passed to the object"""
        return getattr(self.obj, attr)

    def update_methods(self, **methods):
        self.__dict__.update(methods)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
