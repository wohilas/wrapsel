import os, csv
from db_meta.db_abstract import DBAbstract

class DBTSV(DBAbstract):

    name = "tsv"

    def __init__(self, *args, **kwargs):
        # check if file exists
        # create if not

        filepath = kwargs.get("filepath", "closet/default.tsv")
        self.filepath = self.create_if_not_exists(filepath)

    def write_line(self, **line):
        line = {**line}

        # file exists already
        with open(self.filepath, "a", newline="") as tf:
            writer = csv.DictWriter(tf, fieldnames=line.keys(),
                        delimiter="\t")

            #write headers if file is empty
            if os.fstat(tf.fileno()).st_size == 0:
                writer.writeheader()

            #TODO new headers problem
            writer.writerow(line)


    def create_if_not_exists(self, filepath="default.tsv"):
        if not os.path.exists(filepath):
            with open(filepath, "w", newline="") as tsv_file:
                pass
        return filepath
