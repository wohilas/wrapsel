from pymongo import MongoClient
from db_meta.db_abstract import DBAbstract


class DBMongo(DBAbstract):

    name = "mongo"

    def __init__(self, *args, **kwargs):
        self.client = MongoClient() # 'mongodb://localhost:27017/'
        self.db = kwargs.get("db_name", "default_db")
        self.collection = kwargs.get("collection_name", "default")
