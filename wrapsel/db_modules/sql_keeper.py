import sqlite3
from db_meta.db_abstract import DBAbstract

class DBSQL(DBAbstract):

    name = "sqlite"

    def __init__(self, *args, **kwargs):
        filepath = kwargs.get("filepath", "closet/default.sql")
        self.conn = sqlite3.connect(filepath)
        self.cursor = self.conn.cursor()
        self.table = kwargs.get("table", "default_table")

    def write_line(self, **line):
        line = {**line}

        self.create_if_not_exists(cols=tuple(line))

        placeholders = ', '.join(['?'] * len(line))
        columns = ', '.join(line.keys())
        sql = f"INSERT INTO {self.table} ( {columns} ) VALUES ( {placeholders} )"
        print(sql)
        vals = tuple(str(val) for val in line.values())
        print(vals)
        self.cursor.execute(sql, (vals))
        self.conn.commit()

    def create_if_not_exists(self, cols):
        cols_sql = " text, ".join(cols) + " text"
        sql = 'create table if not exists ' + self.table + f' ({cols_sql})'
        print(sql)
        self.cursor.execute(sql)
