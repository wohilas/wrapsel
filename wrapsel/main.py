#!/usr/bin/env python
import selenium.common.exceptions as selenium_exc
from db_modules.data_keeper import DBTSV
from db_modules.sql_keeper import DBSQL
from db_meta.db_adapter import DBAdapter
from parsers.bingoboom import BingoBoomParser
from config.config import read_config
import os, time
import traceback, argparse


def init_flags():
    parser = argparse.ArgumentParser(description="run parser by name")
    parser.add_argument(
        "name",
        nargs="*",
        help="name of the parser to run"
    )
    parser.add_argument(
        "-l",
        dest="list_parsers",
        action="store_true",
        help="lists all the parsers"
    )
    parser.add_argument(
        "-url",
        dest="url to parse",
        help="initial url for parser to try"
    )
    parser.add_argument(
        "-db",
        dest="db_to_use",
        default="tsv",
        help="db module to use"
    )

    if len(os.sys.argv)==1:
        parser.print_help(os.sys.stderr)
        os.sys.exit(1)

    args = parser.parse_args()
    return args



if __name__ == "__main__":
    args = init_flags()
    config = read_config("config/config.toml")
    from parsers.google_search import URL, GoogleMapParser
    from parsers.bingoboom import BingoBoomParser
    from parsers.xbet import XBetParser
    from parsers.instagram import InstaParser
    parsdi = {
        parser.name: parser
        for parser in
        (GoogleMapParser, BingoBoomParser, XBetParser, InstaParser)
    }

    db_di = {
        db.name: db
        for db in
        (DBTSV, DBSQL)
    }

    if args.list_parsers:
        print("parsers:", list(parsdi.keys()))
        print("db modules:", list(db_di.keys()))
        os.sys.exit(0)

    db = db_di[args.db_to_use]()
    adapter = DBAdapter(db)

    passed_name = args.name[0]
    if passed_name not in parsdi:
        print(f"No such parser: {passed_name} exists; try -l flag to see all available parsers")
        os.sys.exit(2)
    parser = parsdi[passed_name](db=adapter, config=config)
    try:
        parser.parse()
        parser.browser.quit()
    except Exception as e:
        print(e)
        traceback.print_exc()
        parser.browser.quit()
