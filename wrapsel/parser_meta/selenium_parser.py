from selenium import webdriver
import selenium, os, time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
import selenium.common.exceptions as selenium_exc

class SeleniumParser():

    def init_driver(self, url):
        options = Options()
        # options.headless = True
        driver_path = os.path.join(os.getcwd(), "closet/geckodriver")
        self.driver = webdriver.Firefox(
                executable_path=driver_path,
                options=options)
        self.driver.get(url)
        return self.driver

    @staticmethod
    def get_element_by_xpath(source, xpath, many=False, tries=3, wait_time=30):
        for i in range(tries):
            try:
                if many:
                    elements = WebDriverWait(source, wait_time).until(
                        EC.presence_of_all_elements_located((By.XPATH, xpath)))
                    return elements

                element = WebDriverWait(source, wait_time).until(
                    EC.element_to_be_clickable((By.XPATH, xpath)))
                return element
            except selenium_exc.TimeoutException as e:
                print(e)
                return None
            except selenium_exc.StaleElementReferenceException as e:
                    print(f"try#{i}:\n", e)
                    time.sleep(.5)
                    continue

    @staticmethod
    def get_element_text(element, tries=3):
        for i in range(tries):
            try:
                text = element.text
                return text
            except selenium_exc.StaleElementReferenceException as e:
                print(f"try#{i}:\n", e)
                time.sleep(.5)
                continue
        return None

    def js_click(self, element):
        print("clicking element:", element)
        self.driver.execute_script("arguments[0].click();",
                element)
