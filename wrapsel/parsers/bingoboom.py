#!/usr/bin/env python
import selenium.common.exceptions as selenium_exc
from db_modules.data_keeper import DBTSV
from db_meta.db_adapter import DBAdapter
from parser_meta.selenium_parser import SeleniumParser
import os, time
import traceback

URL = "https://bingoboom.ru/sport"


class BingoBoomParser(SeleniumParser):

    name = "bingo"

    def __init__(self, db, init_url=URL):
        self.browser = self.init_driver(init_url)

        # DBAdapter
        self.db = db


    def parse(self):
        with open("bingo.html", "w") as lf:
            lf.write(self.browser.page_source)

        # wait for page to load
        iframe = "//*[@id='sport_iframe_1']"
        self.get_element_by_xpath(self.browser, iframe)

        # switch to iframe
        self.browser.switch_to.frame('sport_iframe_1')


        live_bet_xpath = "//div[@id='live_betting']"
        live_bet = self.get_element_by_xpath(self.browser, live_bet_xpath)

        # find every sport div: tg-bg-3 tg--mar-b-16 collapsed sp sp#
        sport_div_xpath = "//div[@class='sports_list']/div"
        s_divs = self.get_element_by_xpath(live_bet,
                xpath=sport_div_xpath, many=True)


        # check if tab visible (display style block or none)
        res_keep = ".//div[@class='tg-bg-3 tg--shadow']"
        for s_div in s_divs:

            sport_name_xpath = ".//div[@class='n tg--oe tg--mar-r-8']"
            sport_name = self.get_element_by_xpath(s_div,
                    xpath=sport_name_xpath)

            if s_div.get_attribute("class") == "sp tg--mar-b-16":
                #self.browser.execute_script("arguments[0].click();",
                 #       sport_name)
                self.js_click(sport_name)
                coords = sport_name.location_once_scrolled_into_view
                #sport_name.click()

            res_div = self.get_element_by_xpath(s_div, res_keep)
            if not res_div:
                print("couldn't get res_keep from:", s_div)
                continue


            # if *** click it
            more_element_xpath = ".//div[@class='tg__checkbox_ico unchecked']"
            more_element = self.get_element_by_xpath(s_div, more_element_xpath)
            if more_element:
                #self.browser.execute_script("arguments[0].moveToElement();",
                 #       more_element)
                #more_element.click()
                self.browser.execute_script("arguments[0].click();",
                                       more_element)
                # TODO after click needs to read rest of the lines


            # take the data
            data_wrapper_xpath = ".//div[@class='wrapper tg__onegame_wrapper']"

            data_line_xpath = ".//div[@class='oneGame tg__one_game tg--pad-8 tg--flex tg--align-center']"
            data_lines = self.get_element_by_xpath(s_div, data_line_xpath, many=True)

            for data_line in data_lines:

                line_to_write = dict()

                line_to_write["sport"] = sport_name.text
                line_to_write["timestamp"] = time.time()

                # order: team_name score team_name score
                score_and_teams_xpath = ".//div[@class='maName tg--flex tg--flex-col tg-100']//span"
                score_and_teams = self.get_element_by_xpath(data_line,
                        score_and_teams_xpath, many=True)
                if score_and_teams:
                    for i, span in enumerate(score_and_teams):
                        if i%2==0:  # score
                            keyname = "score"
                            team_i = i
                        else:   # team name
                            keyname = "name"
                            team_i = i-1
                        line_to_write[f"team_{team_i}_{keyname}"] = \
                            self.get_element_text(span)

                time_xpath = ".//div[@class='tg__home_game_time tg--mar-r-8 tg--ns tg--flex tg--flex-col tg--justify-center tg--align-center']"
                match_time = self.get_element_by_xpath(data_line, time_xpath)
                line_to_write["match_time"] = self.get_element_text(match_time)

                # if not found => odds empty
                # better to take text from the div and split on three
                odds_xpath = ".//div[@class='tg__home_game_odd tg--mar-r-8 tg--flex tg--justify-between tg--ns']"
                odds = self.get_element_by_xpath(data_line, odds_xpath, many=True)
                for i, odd in enumerate(odds):
                    if odd:
                        types = ["p1", "X", "p2"]
                        # need to know order
                        odds_text = self.get_element_text(odd)
                        odds_di = dict(zip(types, odds_text.split("\n")))
                        line_to_write["odds"] = odds_di


                more_xpath = ".//div[@class='tg__more tg__home_game_more tg--ns']"
                more_plus = self.get_element_by_xpath(data_line, more_xpath)
                line_to_write["more_plus"] = self.get_element_text(more_plus)

                # write to db
                self.db.write_line(**line_to_write)

        self.browser.quit()
