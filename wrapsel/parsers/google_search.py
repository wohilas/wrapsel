#!/usr/bin/env python
import selenium.common.exceptions as selenium_exc
from db_modules.data_keeper import DBTSV
from db_meta.db_adapter import DBAdapter
from parser_meta.selenium_parser import SeleniumParser
import os, time
import traceback

CITY = "chelny"
URL = f"https://google.com/maps/search/{CITY}+hotels"

class GoogleMapParser(SeleniumParser):

    name = "g_map"

    def __init__(self, db, init_url=URL):
        self.browser = self.init_driver(init_url)

        # DBAdapter
        self.db = db


    def parse(self):

        scrollable_parent_xpath = "//div[@class='section-listbox section-scrollbox scrollable-y scrollable-show section-listbox-flex-vertical']"
        scrollable_parent = self.get_element_by_xpath(self.browser,
                scrollable_parent_xpath)

        hotel_block_xpath = ".//div[@class='section-result-content']"
        hotel_blocks = self.get_element_by_xpath(scrollable_parent,
                hotel_block_xpath, many=True)

        for hotel_block in hotel_blocks:
            title_xpath = ".//h3[@class='section-result-title']"
            price_xpath = ".//div[@class='section-result-hotel-price']"

            title = self.get_element_by_xpath(hotel_block, title_xpath)
            price = self.get_element_by_xpath(hotel_block, price_xpath)

            if not title or not price:
                continue

            print(title.text, price.text)

        time.sleep(10)

        self.browser.quit()

if __name__ == "__main__":

    #init db
    tsv_db = DBTSV()
    db_methods = {m:getattr(tsv_db, m)
            for m in tsv_db.__dir__()
            if "__" not in m}
    adapter = DBAdapter(tsv_db, **db_methods)

    map_parser = GoogleMapParser(init_url=URL, db=adapter)
    try:
        map_parser.parse()
        map_parser.browser.quit()
    except Exception as e:
        print(e)
        traceback.print_exc()
        map_parser.browser.quit()
