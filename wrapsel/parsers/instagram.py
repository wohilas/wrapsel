#!/usr/bin/env python
import selenium.common.exceptions as selenium_exc
from parser_meta.selenium_parser import SeleniumParser
import os, time
import traceback

URL = "https://www.instagram.com/accounts/login/?source=auth_switcher"

class InstaParser(SeleniumParser):

    name = "instagram"

    def __init__(self, db, config, init_url=URL):
        self.browser = self.init_driver(init_url)
        self.config = config

        # DBAdapter
        self.db = db

    def parse(self):
        self.login()

    def login(self):
        login_input = "//input[@name='username']"
        lp = self.get_element_by_xpath(self.browser, login_input)

        lp.click()
        lp.send_keys(self.config[self.name]["login"])


        pass_input = "//input[@name='password']"
        pp = self.get_element_by_xpath(self.browser, pass_input)
        pp.click()
        pp.send_keys(self.config[self.name]["password"])

        submit_button = "//button[@type='submit']"
        sb = self.get_element_by_xpath(self.browser, submit_button)
        sb.click()

        time.sleep(2)

        with open("insta.html", "w") as lf:
            lf.write(self.browser.page_source)

        # click send email button
        send_mail = "//button[text()='Send Security Code']"
        print(f"searching for send mail button with '{send_mail}' xpath")
        smb = self.get_element_by_xpath(self.browser, send_mail)
        smb.click()


        time.sleep(3)
        self.browser.quit()
