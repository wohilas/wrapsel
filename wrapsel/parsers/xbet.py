#!/usr/bin/env python
import selenium.common.exceptions as selenium_exc
from parser_meta.selenium_parser import SeleniumParser
import os, time
import traceback

SPORT = "Boxing"
URL = f"https://1xstavka.ru/en/line/{SPORT}/"

class XBetParser(SeleniumParser):

    name = "xbet"

    def __init__(self, db, init_url=URL):
        self.browser = self.init_driver(init_url)

        # DBAdapter
        self.db = db


    def parse(self):

        #parent line
        parent_lines_xpath = "//div[@class='c-events__item c-events__item_game']"
        parent_lines = self.get_element_by_xpath(self.browser,
                parent_lines_xpath, many=True)

        for parent_line in parent_lines:

            line_to_write = dict()

            # start time
            time_xpath = ".//div[@class='c-events__time  min']"
            start_time = self.get_element_by_xpath(parent_line, time_xpath)
            if start_time:
                line_to_write["start_time"] = self.get_element_text(start_time)


            # teams
            teams_xpath = ".//span[@class='c-events__teams']"
            team_names = self.get_element_by_xpath(parent_line, teams_xpath)
            try:
                for index, name in enumerate(
                        self.get_element_text(team_names).split("\n")
                ):
                    line_to_write[f"team_{index}"] = name
            except AttributeError as e:
                continue


            #more_plus
            more_plus_xpath = ".//a[@class='c-events__more c-events__more_bets js-showMoreBets']"
            more_plus = self.get_element_by_xpath(parent_line, more_plus_xpath)
            line_to_write["more_plus"] = self.get_element_text(more_plus)

            #odds
            # when fullpage theres 9 values and 6 when its mobile*
            bets_xpath = ".//div[@class='c-bets']"
            odds = self.get_element_by_xpath(parent_line, bets_xpath)
            odds = self.get_element_text(odds).split()

            odds_titles = [
                    "odds_1", "odds_X", "odds_2", "odds_1X",
                    "odds_12", "odds_2X", "odds_O", "odds_Total", "odds_U",
                    ]

            for key, value in zip(odds_titles, odds):
                line_to_write[key] = value

            print(line_to_write)

            self.db.write_line(**line_to_write)

        self.browser.quit()
